#include <stdio.h>
#include <stdlib.h>
#include "complex2real.c"

//parte1
void read_dat_file_float(const char* fname, float *out, int len){
    FILE* fid = fopen(fname,"rb");

    //valida que el archivo exista
    if(fid == NULL){
        printf("ERROR: %s", fname);
        exit(0);
    }

    fread(out, sizeof(float), len, fid);

    printf("F read - arreglo: %d %d - largo: %d", sizeof(out), out, len);
    
    //llamo a la funcion para obtener la parte compleja
    getRealPart_complex2(out,len);

    printf("YA PASE");

    fclose(fid);
}