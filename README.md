# Sistemas Operativos 2/ Laboratorio 1

## I. Objetivos Generales

Este laboratorio tiene como objetivo aplicar los conceptos de creaci ́on de procesos, la comunicaci ́on entre
estos a trav ́es depipesy env ́ıo de se ̃nales a trav ́es del uso de la funci ́on exec e, mediante el soporte de un
sistema operativo basado en el n ́ucleo Linux y el lenguaje de programaci ́on C.

## II. Objetivos Espec ıficos

1. Conocer y usar las funcionalidades degetopt()como m ́etodo de recepci ́on de par ́ametros de entradas.
2. Construir funciones de lectura y escritura de archivos binarios usandoopen(),read(), ywrite().
3. Construir funciones de procesamiento de im ́agenes.
4. Practicar t ́ecnicas de documentaci ́on de programas.
5. Conocer y practicar uso de makefile para compilaci ́on de programas.
6. Crear procesos a trav ́es del uso defork().
7. Ejecutar programas usando alguna de los llamados de la familiaexec()
8. Comunicar procesos mediante pipes, usandopipes

## III. Conceptos

### III.A. Funciones exec

La familia de funcionesexec()reemplaza la imagen de proceso actual con una nueva imagen de proceso. A
continuaci ́on se detallan los par ́ametros y el uso de dichas funciones:

- **int** execl(**const char** *path,**const char** *arg, ...);
    - Se le debe entregar el nombre y ruta del fichero ejecutable
    - Ejemplo:
       execl(”/bin/ls”,”/bin/ls”,”-l”, (**const char** *)**NULL**);
- **int** execlp(**const char** *file,**const char** *arg, ..., (**const char** *)**NULL**);
    - Se le debe entregar el nombre del fichero ejecutable (implementa b ́usqueda del programa).
    - Ejemplo:
       execlp(”ls”,”ls”,”-l”, (**const char** *)**NULL**);
- **int** execle(**const char** *path,**const char** *arg,..., char * const envp[]);
- Se le debe entregar el nombre y ruta del fichero ejecutable, recibe adem ́as un arreglo de strings
    con las variables de entorno
- Ejemplo:
    char*env[] ={”PATH=/bin”, (**const char** *)**NULL**};
    execle(”ls”,”ls”,”-l”,(**const char** *)**NULL**, char *env[]);
- **int** execv(**const char** *path,char *constargv[]);
- Se le debe entregar el nombre y ruta del fichero ejecutable, recibe adem ́as un arreglo de strings
con los argumentos del programa
- Ejemplo:
char*argv[] ={”/bin/ls”,”-l”, (**const char** *)**NULL**};
execv(”ls”,argv);
- **int** execvp(**const char** *file,char *constargv[]);
- Se le debe entregar el nombre del fichero ejecutable (implementa b ́usqueda del programa), recibe
adem ́as un arreglo de strings con los argumentos del programa
- Ejemplo:
char*argv[] ={”ls”,”-l”, (**const char** *)**NULL**};
execvp(”ls”,argv);
- **int** execvpe(**const char** *file,char *constargv[],char *constenvp[]);
- Se le debe entregar el nombre del fichero ejecutable (implementa b ́usqueda del programa), recibe
adem ́as un arreglo de strings con los argumentos del programa y un arreglo de strings con las
variables de entorno
- Ejemplo:
char*env[] ={”PATH=/bin”, (**const char** *)**NULL**};
char*argv[] ={”ls”,”-l”, (**const char** *)**NULL**};
execvpe(”ls”,argv,env);

Tomar en consideraci ́on que por convenci ́on se utiliza como primer argumento el nombre del archivo
ejecutable y adem ́astodoslos arreglos destringdeben tener un puntero**NULL**al final, esto le indica al
computador que debe dejar de buscar elementos.
Otra cosa importante es que la definici ́on de estas funciones vienen incluidas en la bibliotecaunistd.h,
que algunos compiladores la incluyen por defecto, pero en ciertos sistemas operativos deben ser incluidas
expl ́ıcitamente al comienzo del archivo con la sentencia#include<unistd.h>.

### III.B. Función fork

Esta función crea un proceso nuevo o “proceso hijo” que es exactamente igual que el “proceso padre”. Si
fork() se ejecuta con ́exito devuelve:
Al padre: el PID del proceso hijo creado. Al hijo: el valor 0.
Es decir, la ́unica diferencia entre estos procesos (padre e hijos) es el valor de la variable de identificaci ́on
PID.
A continuaci ́on un ejemplo del uso de fork():

## IV. Definición de etapas

La aplicación consiste en unpipelinede procesamiento de im ́agenes astron ́omicas. Una imagen as
tron ́omica es producida por un proceso computacional llamado s ́ıntesis o reconstrucci ́on de im ́agenes **int** er-
ferom ́etricas. Es decir, las antenas no capturan la imagen que nosotros vemos, sino que detectan se ̃nales de
radio, las cuales son transformadas por un programa computacional en la imagen visual.

En estricto rigor los datos recolectados por las antenas corresponden a muestras del espacio Fourier de la
imagen. Estas muestras (visibilidades) est ́an repartidas en forma no uniforme e irregular en el plano Fourier.
La figura 2 muestra un t ́ıpico patr ́on de muestreo del plano Fourier. La imagen de la derecha muestra la ima-
gen reconstruida a partir de los datos. Esta imagen corresponde a un disco protoplanetario llamado HLTau,
siendo reconstruida gracias a una primera etapa donde se aplica la inversa de la transformada Fourier a las
visibilidades, luego, si los datos lo requieren, se procede por obtener la parte real.

En cada punto (u, v) se mide una se ̃nal llamada Visibilidad. Cada visibilidad es un n ́umero complejo, es
decir posee una parte real y otra imaginaria. La imagen por otro lado es solo real.
Por lo tanto, a partir de un archivo binario que contiene la inversa de la transformada de Fourier, se
requiere que cada imagen pase por tres etapas de procesamiento tal que al final delpipelinese clasifique la
imagen como satisfaciendo o no alguna condici ́on a definir. El programa procesar ́a varias im ́agenes, una a
la vez.
Las etapas delpipelineson:

1. Lectura de archivo binario .raw
2. Obtener parte real de los datos
3. Binarizaci ́on de imagen
4. An ́alisis de propiedad
5. Escritura de resultados

### IV.A. Lectura de im ́agenes

Los archivos a leer corresponden a la inversa de la transformada de Fourier aplicado a las muestras del espacio
Fourier capturadas por las antenas. Por lo tanto, los archivos a leer corresponden a n ́umeros complejos, los
cuales estar ́an almacenados en binario con formato raw, con tama ̃no del archivo 2×N×N. Su formato es:
reali, imaginarioi+1, reali+2, imaginarioi+3, ..., ..., real 2 ×N− 2 , imaginario 2 ×N− 1

Se tendr ́annim ́agenes y sus nombres tendr ́an el mismo prefijo seguido de un n ́umero correlativo. Es
decir, los nombres ser ́an imagen1, imagen2, ... , imagenn.
Para este laboratorio se leer ́a una nueva imagen cuando la anterior haya finalizado elpipelinecompleto.
Es decir, en elpipelines ́olo habr ́a una imagen a la vez.

### IV.B. Conversión de n ́umeros complejos a parte real

En las im ́agenes raw a utilizar, cada pixel de la imagen est ́a representado por n ́umeros decimales, donde las
posiciones pares de la imagen (0,2,4,..., 2×N∗N) representan la parte real del n ́umero complejos, mientras
que las posiciones impares representan la parte compleja. Para convertir estos datos complejos a una imagen,
se requiere obtener la parte real, es decir:

```
j= 0, i = 0
Para cada posici ́on i del archivo:
S ́ı i es par:
imagenFinal[j] = archivo[i]
j++
```
### IV.C. Binarizaci ́on de una imagen

Para binarizar la imagen basta con definir un umbral, el cual define cu ́ales p ́ıxeles deben ser transformados
a blanco y cu ́ales a negro, de la siguiente manera. Para cada pixel de la imagen, hacer

```
s i e l p i x e l >UMBRAL
p i x e l = 1
s i n o
p i x e l = 0
```
Los valores 0 y 1 no son representados por un bit sino son valores enteros (int) Al aplicar el algoritmo
anterior, obtendremos la imagen binarizada.

### IV.D. Clasificación

Se debe crear una funci ́on que concluya si la imagen esnearly black (casi negra). Esta caracter ́ıstica es
t ́ıpica de muchas im ́agenes astron ́omicas donde una peque ̃na fuente puntual de luz est ́a rodeada de vacio u
oscuridad. Entonces, si el porcentaje de p ́ıxeles negros es mayor o igual a un cierto umbral la imagen es
clasificada comonearly black.
El programa debe imprimir por pantalla la clasificaci ́on final de cada imagen y escribir en disco la imagen
binarizada resultante.

## V. Uso de conceptos de procesos

Para ocupar funciones propias de procesos en este contexto, cada etapa delpipelinedebe ejecutarse por
un proceso ́unico. Cada proceso se comunica con el siguiente por medio depipes, entreg ́andole su respectiva
salida. A continuaci ́on se muestra un diagrama explicativo de esta estrategia:

Para la creaci ́on de procesos debe realizarse s ́ı o s ́ı con uso defork(). Adem ́as, es importante destacar
que unpipes ́olocomunica procesos que compartan el pipe correspondiente. Por ejemplo, Si un padre crea
un pipe y luego crea un hijo, entonces ellos podran comunicarse. Si un padre crea un pipe y luego dos hijos,
ambos hijos podr ́an comunicarse tambi ́en entre ellos.
El proceso Main s ́olo debe terminar una vez que todos los otros procesos hayan acabado.

## VI. Otros conceptos

- Abrir el archivo con el uso de open(). Recuerde que este archivo debe contener la matriz (imagen).
- Leer la imagen con read().

Con los pasos previamente descritos, se puede leer la imagen y manipularla de tal forma que se pueda
crear la matriz. Recuerde que la imagen tiene una estructura para poder obtener el valor de los pixeles, por
lo que se pide investigar al respecto.
Para el caso de escribir la imagen resultante:

- Se debe escribir con write().
- Finalmente se debe cerrar el archivo con close().

Por ́ultimo, el programa debe recibir la cantidad de im ́agenes a leer, el valor del umbral de binarizaci ́on,
el valor del umbral de negrura y una bandera que indica si se desea mostrar o no la conclusi ́on final hecha
por la tercera funci ́on. Por lo tanto, el formato getopt es:

- -c: cantidad de im ́agenes
- -N: cantidad de filas o columnas de las im ́agenes
- -u: UMBRAL para binarizar la imagen.
- -n: UMBRAL para clasificaci ́on
- -b: bandera que indica si se deben mostrar los resultados por pantalla, es decir, la conclusi ́on obtenida
    al leer la imagen binarizada.
```
$ ./pipeline -c 3 -u 50 -n 80 -b
```
| image | nearly black |
|--------------|---------------------|
| imagen_1 | yes |
| imagen_2 | no |
| imagen_3 | no |

Donde s ́olo la imagen1 fue clasificada comonearly black.

## VII. Entregables

Debe entregarse un archivo comprimido que contenga al menos los siguientes archivos:

1. Makefile: archivo para make que compile los programas.
2. dos o mas archivos con el proyecto (*.c, *.h)

El archivo comprimido debe llamarse: RUTESTUDIANTE1RUTESTUDIANTE2.
Ejemplo: 19689333k189225326.zip

## VIII. Fecha de Entrega

Domingo 17 de Octubre a las 23:55 hrs.


