#include <stdio.h>
#include <stdlib.h>
#include "read.c"

//parte 0

int main(){

    char nameFile[20] = "ifft_tworings.raw";
    // char nameFileOut[25] = "salida.raw";
    // int size = 1024;
    int size =32;
    int lenComplex = 2*size*size;

    printf("Antes de leer\n");

    //asignar memoria
    float *Visibilidades = (float*)malloc(sizeof(float)*lenComplex);
    //deberia sacar esta asignacion
    float *VisibilidadesOut = (float*)malloc(sizeof(float)*lenComplex);

    //llamo a la funcion leer
    read_dat_file_float(nameFile, Visibilidades, lenComplex);
    // write_dat_file_float(nameFileOut, VisibilidadesOut, lenComplex);

    printf("Despues de Escribir\n");
    
    //libera memoria
    free(VisibilidadesOut);
    free(Visibilidades);

    return 0;

}